## Ref: Full Stack Web Development with Flask

https://www.linkedin.com/learning/full-stack-web-development-with-flask/creating-navigation-links-and-route-patterns?autoAdvance=true&autoSkip=true&autoplay=true&resume=false&u=56982905


## 2. Creating a Falsk Project: Navigation Links and Routing Patterns
* Creating navigation menus
* Using the url_for function to resolve links
* Using the route() decorator to bind a function to one or more URL patterns
    ```python
    http://domain.com/
    http://domain.com/index
    http://domain.com/home
    ```
* Jinja delimiters
    ```python
    {% ... %} - Statements
    {{ ... }} - Expressions
    {# ... #} - Comments
    ```
* The if statement in Jinja



## 3. Working with Template
* base template:
    * Jinja template inheritance logic
    * Creating the base template, e.g. layout.html
    * using template inheritance to create child templates, e.g. block content, and can have multiple child templates
    
* Parsing data into the view using props:
    * parsing data from the source to the view
    * Highlight teh active menue item
    * using **For** directive (for  endfor block)
    * build the course table with JSON data

* accessing data via teh request and response objects
    * URL varibles (routing patterns, creating URL variable, setting default data to a URL variable, passing a URL variable to a template)
    * HTTP methods (GET, POST)
    * the global objects: **Request** and **Respons**
    * requests and respons are all JSON API format
* The Request object
    * accessing Query string (GET)
        ```python
        request.args.get(<field_name>)
        request.args[<field_name>]
        ```
    * accesing form data (POST)
        ```python
        request.form.get(<filed_name>)
        request.form[<filed_name>]
        ```
* The Response object
    ```python
    class flask.Response(
        response=None,     # commonly used para
        status=None,
        headers=None,
        mimetype=None,     # commonly used
        content_type=None, # commonly used
        direct_passthrough=False
    )
    ```
* GET and POST methods
    NOTE: change method into POST for form enrollment
    --> method not allowed (the method is not allowed for the request URL)
    --> update the request method in enrollment route (function)

* JSON response
    ```python
    class flask.Response(
        response=None,         # commonly used parameter
        status=None,
        headers=None,
        mimetype=None,         # commonly used parameter
        content_type=None,     # commonly used parameter
        direct_passthrough=False
    )
    ```

## 4. Flask working with Database
* Install the MongoDB database system: MongoDB Community Service
* Install the MongoEngine extension for Flask
    ```python
    pip install flask-mongoengine
    ```
* Set up the database
    ```python
    MONGODB_SETTINGS = { 'db': 'UTA_Enrollment'} # setting up MongoDB database
    from flask_mongoengine import MongoEngine    # importing the MongoEngine

    db = MongoEngine()                           # initialize the database object
    db.init_app(app)                             # pass application into init_app()
    ```
* Connect to the database

    a. connect to theMongoDB via the MongoEngine object

    b. Hook up a user document (data) to a collection

    c. Insert sample user document (data) to a collection

    d. Display the collection to the view (page)
* Create documents and data

    a. Create collection to store documents (data)

    b. Insert documents into our collections
    c. Use MongoDB shell commands
        ```python
        db.createCollection(<collection>)
        db.<collection>.insert({ ... })    # insert single or many
        db.<collection>.insertMany({ ... })
        ```
    d. Insert JSON data suing the mongoimport.exe via command line
        ```python
        mongoimport --db <DB> --collection <collection> --file <file>
        mongoimport --d <DB> --c <collection> --file <file>
        ```

* Create data model for application

    create the models module: **User**, **Course** and **Enrollment** models
    ```python 
    class ModelName(db.Document):
        field1 = db.IntField()
        field2 = db.StringField()
        ...
        fieldn = db.StringField()
    ```

## 5. Web forms and Flask security
NOTE: install and configure two extensions
```python
pip install flask-wtf
pip install flask-security
```
* Flask-WTF extension and 
    * Flask-WTF is an extension for the *WTForms* library
    * WTFroms provides a clean way to generate **HTML** form fields
    * Maintain a separation of code and presentation
    ```html
    <!--example of WTForms-->
    <form>
        {{ form.hidden_tag() }} <!-- hidden_tag() function -->
        {{ form.username }}
        {{ form.email }}
        {{ form.password }}
    </form>
    <!--Example of standard HTML Form-->
    <form>
        <input id="csrf_token"
               type="hidden"
               name="csrf_token"
               value="ImFmMzg50...GU4Yjkw">
        <input type="text" name="username">
        <input type="email" name="email">
        <input type="password" name="password">
    </form>
    ```
* Flask-Security extension
    * Provides common security and authetication features
        * Session-based authentication
        * Password hashing
        * Basic HTTP and token-based authentications
        * User registration
        * Login tracking (Flask-Login)
    * Supports data persistency for Falsk-SQLAIchemy, Flask-MongoEngine, Flask-peewee, and PonyORM

* create the login and registration page
    * create form classes (*form.py*) and update the templates (*login* route and page) using WTForms library
    * create alert messages using the **flash()** method
        * create alert message using **flash()** method in source
        * retrieve flash messages using the **get_flashed_messages()** in view
        * specify the catagories of message in flash("message", "category")
    * validate forms and show error message via form
        * Validate the form data
        * show inline error message for form fields
        * style and format error message

        ```python
        # forms.py
        class LoginForm(FlaskForm):
            email    = StringField("Email")
            password = StringField("Password")
            submit   = SubmitField("Login")
        ```

        ```html
        <!-- login.html -->
        <form>
            {{ form.hidden_tag() }}
            {{ form.email }}
            {{ form.password }}
            {{ form.submit }}
        </form>
        ```

* Process **Form** data and update the database
    * Form data validation
    * Process form data for database update
    * hash password using **Werkzeug** library (a WSGI web application librray)
        * **Hashing**: *generate_password_hash("password")*
        * **Unhashing**: *check_password_hash(password, "password")*
* Create the courses and enrollment pages
    * course enrollment process
    * performe join queries on multiple collections using **$aggregate**
        $lookup: Performe a left outer join
        $match: Filters documents
        $unwind: Deconstructs an array filed
    ```python
    # Aggregation Pipline Stages
    # User.objects.aggregate(*[])
    {
        "$lookup": {
            "from": "enrollment",
            "localFiled": "user_id",
            "foreignField": "user_id",
            "as": "resultSet1"
        }
    },
    {
        "$unwind": {
            "path": "$resultSet1",
            "includeArrayIndex": "r1_id",
            "preserveNullAndEmptyArrays": False
        }
    }    
    ```
    
    * add the aggregation pipline to application
        * update the enrollment route to interact with the database
        * integrate the aggregation pipline into the application
        
* create sessions and authentication
    * State management and user authentication using **Flask-Session**
        * Session ans statement management using **Flask-Login** extension
        * manage user logged-in state using a *user_loader()* function
        * Using the **LoginManager** class to Manage login state
        * Implement the "remeber me" feature
        * restrict access to protected pages with *@login_required*
        * Logging out users using the *logout_user()* function

    * The **session** object stores information specific to a user
    * implementation on top of cookies and signs cookies cryptographically

    ```python
    # https://flask-login.readthedocs.io/en/latest/
    # UrlSegment via the Parameters Property
    session["key"] = value         # setting a session
    session.get("key")             # getting a session

    session.pop("key", None)       # destroying a session
    session["key"] = False         # destroying a session

    # active sessions across multiple pages: Home, Courses and About
    ```

    * configure the navigation to use sessions and test the sessions
    
## 6. Create and Test REST API using Postman
* Create REST APIs
* Install **Flak-RESTPlus Extension**
    * Falsk extension that adds support for creating REST APIs
    * work with Python 2.7 or higher and JIT compiler PyPy and PyPy3
    * very minimal setup
    * Provide a coherent collection of decorators and tools to describe APIs
    * Decorate classes and methods
    * multiple endpoints are allowed in the *route()* decorators
    * Built-in support for request data validation

* install and use Postman for testing our APIs
    * Postman development environment for testing APIs
    * Chrome's HTTP client
    * Most-used REST client worldwide
    * Automate testing with collection runner
    * API monitoring (https://www.postman.com/)

    ```python
    # download Postman into local
    # curl: Client URL
    pip install flask-restx, 
    # NOT flask-restplus, migrate
    ```

* create and test REST APi CRUD operations using HTTP verbs