import os

# secret_string by command:
# python -c "import os; print(os.urandom(16))"
# replace "secrst_string using the above output in SECRET_KY"
class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or b"z\xb6'N\xfa\x18\x91\x1b\x19B_\xea\x8c\xff\xcf\xbb"
    MONGODB_SETTINGS = {'db': 'UTA_Enrollment'
        # 'host': 'mongodb://localhost:27017/UTA_Enrollement' # optional setting
    }