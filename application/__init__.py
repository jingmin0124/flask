from attr import validate
from flask import Flask
from config import Config
from flask_mongoengine import MongoEngine
from flask_restx import Api

api = Api()   # create api instance

app = Flask(__name__)
app.config.from_object(Config)  #  starting app will load the Config data

# MongoEngine(app=None, config=None): initialization of Flask-MongoEngine
db = MongoEngine()
db.init_app(app)
api.init_app(app)    # initialize api by app

from application import routes