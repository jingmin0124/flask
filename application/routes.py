from application import app, api
from flask import jsonify, redirect, render_template, request, flash, url_for, session, jsonify
from application.models import User, Course, Enrollment
from application.form import LoginForm, RegisterForm
from flask_restx import Resource
from application.courses_list import courses_list

###########################################################
@api.route("/api", "/api/")
class GetAndPost(Resource):
    # GET all data
    def get(self):
        return jsonify(User.objects.all())
    
    # POST
    def post(self):
        data = api.payload    # return None for data???
        print(data)
        user = User(user_id=data['user_id'], email=data['email'], first_name=data['first_name'], last_name=data['last_name'])
        user.set_password(data['password'])
        user.save()
        
        return jsonify(User.objects(user_id=data['user_id']))
    
@api.route("/api/<idx>")
class GetUpdateDelete(Resource):
    # GET one piece of idx-based data
    def get(self, idx):
        return jsonify(User.objects(user_id=idx))
    
    # PUT
    def put(self, idx):
        data = api.payload     # data is None???
        print(data)
        User.objects(user_id=idx).update(**data)
        return jsonify(User.objects(user_id=idx))
    
    # DELETE
    def delete(self, idx):
        User.objects(user_id=idx).delete()
        return jsonify("user is deleted")
    
###########################################################


@app.route("/")
@app.route("/index")
@app.route("/home")
def index():
    return render_template("index.html",  title="Home", index=True)

@app.route("/login", methods=["GET", "POST"])
def login():
    if session.get("username"):
        return redirect(url_for("index"))
    form = LoginForm()
    if form.validate_on_submit() == True:
        email = form.email.data
        password = form.password.data
        
        user = User.objects(email=email).first()
        if user and user.get_password(password):
            flash(f"{user.first_name}, you are successfully logged in!", "success")
            # set session by user_id and username
            session["user_id"] = user.user_id
            session["username"] = user.first_name
            return redirect("/index")       
        else:
            flash("Sorry, something went wrong.", "danger")
        
    return render_template("login.html", title="Login", form=form, login=True)

@app.route("/logout")
def logout():
    session["user_id"] = False
    session.pop("username", None)
    return redirect(url_for("index"))

@app.route("/courses")
@app.route("/courses/<term>")
def courses(term=None):  # term: URL variable
    if term == None:
        term = "Spring 2019"
    classes = Course.objects.order_by("title")
    return render_template("courses.html",  title="Courses", coursesData=classes, courses=True, term=term)

@app.route("/register", methods=["GET", "POST"])
def register():
    if session.get("username"):
        return redirect(url_for("index"))
    form = RegisterForm()
    if form.validate_on_submit():
        user_id = User.objects.count()
        user_id += 1
        
        email = form.email.data
        password = form.password.data
        first_name = form.first_name.data
        last_name = form.last_name.data
        
        user = User(user_id=user_id, email=email, first_name=first_name, last_name=last_name)
        user.set_password(password)
        user.save()
        flash("You are successfully registered!", "success")
        return redirect(url_for("index"))
    return render_template("register.html", title="Register", form=form, register=True)

@app.route("/enrollment", methods=["GET", "POST"])
def enrollment():    
    if not session.get("username"):
        return redirect(url_for("login"))
    
    course_id = request.form.get('course_id') 
    # id = request.form["course_id"] 
    course_title = request.form.get('title')
    user_id = session.get("user_id")
    
    if course_id:
        if Enrollment.objects(user_id=user_id, course_id=course_id):
            flash(f"Oops, you are registered in this course {course_title}!", "danger")
            return redirect(url_for("courses"))
        else:
            Enrollment(user_id=user_id, course_id=course_id).save()
            flash(f"You are enrolled in {course_title}!", "success")
    classes = courses_list(user_id)
    return render_template("enrollment.html", title="Enrollment", enrollment=True, classes=classes)

# @app.route("/api")
# @app.route("/api/<idx>")
# def api(idx=None):
#     if idx == None:
#         jdata = Course.objects.all()
#     else:
#         jdata = Course.objects.all()[int(idx)]  # idx in string format

#     return Response(json.dumps(jdata), mimetype="application/json")

# @app.route("/user")
# def user():
#     # User(user_id=6, first_name="Jessica", last_name="Zhou", email="jmzh119yjm@gmail.com", password="123456").save()
#     # User(user_id=5, first_name="James", last_name="Jiang", email="123456@gmail.com", password="abc456").save()

#     users = User.objects.all() # retrieve all the User object from collection in UTA_Enrollement

#     return render_template("user.html", users=users)

