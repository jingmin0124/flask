from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Email, Length, EqualTo, ValidationError
from application.models import User

class LoginForm(FlaskForm):
    email       = StringField("Email", validators=[DataRequired(), Email()])  # "Email" is label
    password    = PasswordField("Password",validators=[DataRequired(), Length(min=6, max=12)])
    remember_me = BooleanField("Remember Me")
    submit      = SubmitField("Login")
    
class RegisterForm(FlaskForm):
    email      = StringField("Email", validators=[DataRequired()])
    password   = PasswordField("Password", validators=[DataRequired(), Length(min=6, max=12)])
    password_confirm   = PasswordField("Confirm Password", validators=[DataRequired(), Length(min=6, max=12), EqualTo('password')])
    first_name = StringField("First Name", validators=[DataRequired(), Length(min=2, max=30)])
    last_name  = StringField("Last Name", validators=[DataRequired(), Length(min=2, max=30)])
    submit     = SubmitField("Register Now")
    
    def validate_email(self, email):
        user = User.objects(email=email.data).first() # first currence
        if user:
            raise ValidationError("Email is already in use. Pick another one.")